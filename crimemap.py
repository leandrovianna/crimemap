import datetime
import json
import string
import dateparser
from dbhelper import DBHelper
from flask import Flask
from flask import render_template
from flask import request


app = Flask(__name__)
db = DBHelper()

categories = ['mugging', 'break-in']


@app.route('/')
def home(error_message=None):
    crimes = db.get_all_crimes()
    crimes_json = json.dumps(crimes)
    return render_template('home.html', crimes=crimes_json,
                           categories=categories, error_message=error_message)


@app.route('/submitcrime', methods=['POST'])
def submitcrime():
    category = request.form.get('category')
    if category not in categories:
        return home()

    date = format_date(request.form.get('date'))
    if not date:
        return home('Invalid date. Please use yyyy-mm-dd format.')

    try:
        latitude = float(request.form.get('latitude'))
        longitude = float(request.form.get('longitude'))
    except ValueError:
        return home()

    description = sanitize_string(request.form.get('description'))
    db.add_crime(category, date, latitude, longitude, description)
    return home()


def format_date(userdate):
    date = dateparser.parse(userdate)
    try:
        return datetime.datetime.strftime(date, '%Y-%m-%d')
    except TypeError:
        return None


def sanitize_string(userinput):
    whitelist = string.ascii_letters + string.digits + " !?$.,;:-'()&"
    return ''.join([x for x in userinput if x in whitelist])
    # return filter(lambda x: x in whitelist, userinput)


if __name__ == "__main__":
    app.run(port=5000, debug=True)
